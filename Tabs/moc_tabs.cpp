/****************************************************************************
** Meta object code from reading C++ file 'tabs.h'
**
** Created: Tue May 7 20:04:39 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "tabs.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tabs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TabMAIN[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x08,
      16,    8,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TabMAIN[] = {
    "TabMAIN\0\0Sync()\0SetLimit(int)\0"
};

void TabMAIN::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TabMAIN *_t = static_cast<TabMAIN *>(_o);
        switch (_id) {
        case 0: _t->Sync(); break;
        case 1: _t->SetLimit((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TabMAIN::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabMAIN::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabMAIN,
      qt_meta_data_TabMAIN, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabMAIN::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabMAIN::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabMAIN::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabMAIN))
        return static_cast<void*>(const_cast< TabMAIN*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabMAIN*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabMAIN::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_TabGGA[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabGGA[] = {
    "TabGGA\0"
};

void TabGGA::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabGGA::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabGGA::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabGGA,
      qt_meta_data_TabGGA, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabGGA::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabGGA::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabGGA::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabGGA))
        return static_cast<void*>(const_cast< TabGGA*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabGGA*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabGGA::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_TabGLL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabGLL[] = {
    "TabGLL\0"
};

void TabGLL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabGLL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabGLL::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabGLL,
      qt_meta_data_TabGLL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabGLL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabGLL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabGLL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabGLL))
        return static_cast<void*>(const_cast< TabGLL*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabGLL*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabGLL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_TabGSA[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabGSA[] = {
    "TabGSA\0"
};

void TabGSA::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabGSA::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabGSA::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabGSA,
      qt_meta_data_TabGSA, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabGSA::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabGSA::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabGSA::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabGSA))
        return static_cast<void*>(const_cast< TabGSA*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabGSA*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabGSA::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_TabGSV[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabGSV[] = {
    "TabGSV\0"
};

void TabGSV::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabGSV::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabGSV::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabGSV,
      qt_meta_data_TabGSV, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabGSV::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabGSV::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabGSV::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabGSV))
        return static_cast<void*>(const_cast< TabGSV*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabGSV*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabGSV::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_TabRMC[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabRMC[] = {
    "TabRMC\0"
};

void TabRMC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabRMC::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabRMC::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabRMC,
      qt_meta_data_TabRMC, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabRMC::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabRMC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabRMC::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabRMC))
        return static_cast<void*>(const_cast< TabRMC*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabRMC*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabRMC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_TabVTG[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TabVTG[] = {
    "TabVTG\0"
};

void TabVTG::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TabVTG::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TabVTG::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TabVTG,
      qt_meta_data_TabVTG, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TabVTG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TabVTG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TabVTG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TabVTG))
        return static_cast<void*>(const_cast< TabVTG*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< TabVTG*>(this));
    return QWidget::qt_metacast(_clname);
}

int TabVTG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_Tabs[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_Tabs[] = {
    "Tabs\0"
};

void Tabs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Tabs::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Tabs::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Tabs,
      qt_meta_data_Tabs, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Tabs::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Tabs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Tabs::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Tabs))
        return static_cast<void*>(const_cast< Tabs*>(this));
    if (!strcmp(_clname, "TabsCommon"))
        return static_cast< TabsCommon*>(const_cast< Tabs*>(this));
    return QDialog::qt_metacast(_clname);
}

int Tabs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
